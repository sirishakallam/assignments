package amazon_webpages;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Flipkartpages {
	 public String s1="";
	public static WebDriver driver;
	 By flipkart_cancel =By.xpath("//*[@class='_3Njdz7']//descendant::button");
	 By flipkart_search_bar =By.name("q");
	 By flipkart_phone_selection =By.xpath("//*[@class='col col-7-12']//descendant::div[1]");
	 By flipkart_get_phone_price =By.xpath("//*[@class='_1uv9Cb']//child::div[1]");
	 
	 public Flipkartpages (WebDriver driver2) {
		 this.driver=driver2;
	
	 }
	
	 public void flipkartsearchphone(String text) {
		 //cancel the popup
		 driver.findElement(flipkart_cancel).click();
		 //search the phone and enter
		 driver.findElement(flipkart_search_bar).sendKeys(text,Keys.RETURN);
		 //select the first phone
		 driver.findElement(flipkart_phone_selection).click();
		 driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		 //to get the window handles
		 Set <String> a = driver.getWindowHandles();
		 System.out.print(a);
		 Iterator<String> it= a.iterator();
		 //get the parent window handle
		 String flipkart_patentwindowid = it.next();
		 String flipkart_childwindowid = it.next();
		 driver.switchTo().window(flipkart_childwindowid);
		 System.out.println("chaildwindowtitle:" +driver.getTitle());
		 driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		 //get the phone price in amazon site
		 WebElement p1 = driver.findElement(flipkart_get_phone_price);
		 s1 = p1.getText();
		System.out.println("flipkart price:"+s1);
		//return flipkart_childwindowid;
	
	 }

}
