package amazon_webpages;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Pages {
	public String s="";
	public WebElement p;
	public WebDriver driver;
	 By search_bar =By.xpath("//*[@type='text' and @id='twotabsearchtextbox']");
	 By phone_selection =By.xpath("//a[@class='a-link-normal a-text-normal']//following::span[1]");
	 By get_phone_price =By.xpath("//*[@class='a-span12']//following::span[1]");
	 
	 public Pages (WebDriver driver1) {
		 this.driver=driver1;
	
	 }
	 
	 public void searchphone(String text) {
		 //search the phone and enter
		 driver.findElement(search_bar).sendKeys(text,Keys.RETURN);
		 //select the first phone
		 driver.findElement(phone_selection).click();
		 driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		 //to get the window handles
		 Set <String> a = driver.getWindowHandles();
		 System.out.print(a);
		 Iterator<String> it= a.iterator();
		 //get the parent window handle
		 String patentwindowid = it.next();
		 String childwindowid = it.next();
		 driver.switchTo().window(childwindowid);
		 System.out.println("chaildwindowtitle:" +driver.getTitle());
		 driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		 //get the phone price in amazon site
		 WebElement p = driver.findElement(get_phone_price);
		String s = p.getText();
		System.out.println("price of amazon:"+s);
		 
	 }
	 

}
