package searchpages;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class Hotelsearchpage {
	
	By scearch_bar = By.xpath("//*[@class='R1IsnpX3 NI9CbWSB']//descendant::input[1]");
	By select_hotel =By.xpath("//*[@class='result-title']//descendant::span[1]");
	By write_review =By.linkText("Write a review");
	By give_rate = By.xpath("//span[@id='bubble_rating' and @data-value=\"5\"]");
	By title_of_review = By.xpath("//*[@name='ReviewTitle' and @class=\"text\"]");
	By your_review = By.xpath("//*[@name='ReviewText']");
	By service_rating =By.xpath("//*[@id='qid12_bubbles' and @data-name='Service']");
	By sleep_quality_rating =By.xpath("//*[@id='qid190_bubbles' and @data-name='Sleep Quality']");
	By value_rating =By.xpath("//*[@id='qid13_bubbles' and @data-name='Value']");
	By check_box= By.xpath("//*[@class='willing ']//descendant::input[2]");
	By click_submit =By.xpath("//*[@id='SUBMIT ']");
	public WebDriver driver;
	
	public Hotelsearchpage (WebDriver driver2) {
		 this.driver=driver2;
	
	 }
	 
	 public void search_hotel_club(String text) {
		 
		 //to search the hotel and enter
		 driver.findElement(scearch_bar).sendKeys(text,Keys.ENTER);
		 //driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		 //to select the hotel
		 driver.findElement(select_hotel).click();
		 driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		 //to handles the windows
		 Set <String> a = driver.getWindowHandles();
		 System.out.print(a);
		 Iterator<String> it= a.iterator();
		 //get the parent window handle
		 String patentwindowid = it.next();
		 String childwindowid = it.next();		 
		 driver.switchTo().window(childwindowid);
		 driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		 //to click the wirte review link
		 driver.findElement(write_review).click();
		 driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		 String childwindowid2 = it.next();
		 driver.switchTo().window(childwindowid2);
		 //to give the rating
		 Actions a1 = new Actions(driver);
		 a1.moveToElement(driver.findElement(give_rate)).build().perform();
		 a1.click();
		 //driver.findElement(give_rate).click();
		 driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		 //write the title of review
		 driver.findElement(title_of_review).sendKeys("good");
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 //write the review
		 driver.findElement(write_review).sendKeys("the hotel so nice and cool");
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 //to give the service,quality and value rating
		 a1.moveToElement(driver.findElement(service_rating)).build().perform();
		 a1.click();
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 a1.moveToElement(driver.findElement(sleep_quality_rating)).build().perform();
		 a1.click();
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 a1.moveToElement(driver.findElement(value_rating)).build().perform();
		 a1.click();
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 //to check the check box and click the submit button
		 driver.findElement(check_box).clear();
		 driver.findElement(click_submit).click();
		 
		 
	 }
	

}
